function [J, grad] = linearRegCostFunction(X, y, theta, lambda)
%LINEARREGCOSTFUNCTION Compute cost and gradient for regularized linear 
%regression with multiple variables
%   [J, grad] = LINEARREGCOSTFUNCTION(X, y, theta, lambda) computes the 
%   cost of using theta as the parameter for linear regression to fit the 
%   data points in X and y. Returns the cost in J and the gradient in grad

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost and gradient of regularized linear 
%               regression for a particular choice of theta.
%
%               You should set J to the cost and grad to the gradient.
%
hypothesisRes = (theta' * X')';

left = (1/(2*m))* sum( (hypothesisRes - y).^2 );
J = left +  (lambda / (2*m) ) * sum( theta(2:end).^2 );

grad = ((1/m)* sum( (hypothesisRes - y).* X ))';
reg = (lambda / m)*theta(2:end);
%The following commented implementaion does not pass the grader. Reason unknown for now.
%grad(2:end) = grad(2:end) + reg;
grad2 = grad;
grad2(2:end) = grad2(2:end) + reg;
reg = [0; reg];
grad = grad + reg;
ress = eq(grad, grad2);

disp("---------------------------->Jianfeng: "), disp(ress);

%disp ("The value of grad and grad2 is:"), disp (grad), disp(grad2);


%following is copied from https://github.com/benoitvallon/coursera-machine-learning/blob/master/machine-learning-ex5/ex5/linearRegCostFunction.m
%mask = ones(size(theta));
%mask(1) = 0;
%grad = 1 / m * ((X * theta - y)' * X)' + lambda / m * (theta .* mask);

% =========================================================================
grad = grad(:);

end
